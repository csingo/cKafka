package cKafka

import (
	"gitee.com/csingo/cComponents"
)

type KafkaComponent struct{}

func (i *KafkaComponent) Inject(instance any) bool {
	return false
}

func (i *KafkaComponent) InjectConf(config cComponents.ConfigInterface) bool {
	if config.ConfigName() == KafkaConfigName {
		kafka_config = config.(*KafkaConf)
		return true
	}

	return false
}

func (i *KafkaComponent) Load() {
	load()
}

func (i *KafkaComponent) Listen() []*cComponents.ConfigListener {
	return []*cComponents.ConfigListener{}
}

var Component = &KafkaComponent{}
