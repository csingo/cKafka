package cKafka

import "github.com/gin-gonic/gin"

type KafkaOperatorInterface interface {
	ID() string
	Close(ctx *gin.Context)
	Delete()
}
