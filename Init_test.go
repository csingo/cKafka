package cKafka

import (
	"log"
	"testing"
	"time"

	"github.com/IBM/sarama"

	"gitee.com/csingo/cContext"
	"gitee.com/csingo/cLog"
)

func TestLoad(t *testing.T) {
	ctx := cContext.New()

	cLog.InjectConf(&cLog.LogConf{
		Level:      cLog.TraceLevel,
		Drivers:    []cLog.LogDriver{"file"},
		WithFields: nil,
		Clients: &cLog.LogConf_Clients{
			File: &cLog.LogConf_FileClient{Path: "/dev/stdout"},
			Loki: nil,
		},
	})
	cLog.Load()

	selfKafkaConf.HealthCheckInterval = 10
	selfKafkaConf.Connections["default"] = &KafkaConf_Connection{
		Brokers: []string{"172.16.17.120:9092"},
		Topic:   "cxytest",
		Net: &KafkaConf_Connection_Net{
			MaxOpenRequests: 10,
			Keepalive:       0,
			DialTimeout:     0,
			ReadTimeout:     0,
			WriteTimeout:    0,
			TLS: &KafkaConf_Connection_Net_TLS{
				Enable: false,
				Skip:   false,
				Cert:   "",
				Key:    "",
			},
		},
		Producer: &KafkaConf_Connection_Producer{
			MaxMessageBytes: 0,
			RequiredAck:     true,
			Partitioner:     PRODUCER_PARTITIONER_MANUAL,
			Partition:       2,
		},
		Consumer: &KafkaConf_Connection_Consumer{
			Offset:     sarama.OffsetNewest,
			Partitions: []int32{0},
			Group:      "cxytest_group",
			Size:       0,
			Wait:       5,
		},
	}
	Load()

	log.Println("start")

	p, _ := GetSyncProducer("default")
	p.Produce(ctx, []*ProducerMessage{
		{
			Key:     "",
			Value:   "cxy",
			Headers: nil,
		},
	})

	log.Println("end")
	time.Sleep(600 * time.Second)
}
