package cKafka

import "github.com/IBM/sarama"

const KafkaConfigName = "KafkaConf"

type ProducerPartitioner string

const (
	PRODUCER_PARTITIONER_HASH   ProducerPartitioner = "hash"
	PRODUCER_PARTITIONER_RANDOM ProducerPartitioner = "random"
	PRODUCER_PARTITIONER_MANUAL ProducerPartitioner = "manual"
)

const (
	CONSUMER_OFFSET_OLDEST = sarama.OffsetNewest
	CONSUMER_OFFSET_NEWEST = sarama.OffsetOldest
)

type KafkaConf struct {
	Connections map[string]*KafkaConf_Connection `json:"connections"`
	HealthCheck int64                            `json:"health_check"`
}

type KafkaConf_Connection struct {
	Brokers  []string                       `json:"brokers"`
	Topic    string                         `json:"topic"`
	Net      *KafkaConf_Connection_Net      `json:"net"`
	Producer *KafkaConf_Connection_Producer `json:"producer"`
	Consumer *KafkaConf_Connection_Consumer `json:"consumer"`
	Version  [4]uint                        `json:"version"`
}

type KafkaConf_Connection_Net struct {
	MaxOpenRequests int                           `json:"max_open_requests"`
	Keepalive       int64                         `json:"keepalive"`
	DialTimeout     int64                         `json:"dial_timeout"`
	ReadTimeout     int64                         `json:"read_timeout"`
	WriteTimeout    int64                         `json:"write_timeout"`
	TLS             *KafkaConf_Connection_Net_TLS `json:"tls"`
}

type KafkaConf_Connection_Net_TLS struct {
	Enable bool   `json:"enable"`
	Skip   bool   `json:"skip"`
	Cert   string `json:"cert"`
	Key    string `json:"key"`
}

type KafkaConf_Connection_Producer struct {
	MaxMessageBytes int                 `json:"max_message_bytes"`
	RequiredAck     bool                `json:"required_ack"`
	Partitioner     ProducerPartitioner `json:"partitioner"`
	Partition       int32               `json:"partition"`
}

type KafkaConf_Connection_Consumer struct {
	Hearbeat   int64   `json:"hearbeat"`
	Offset     int64   `json:"offset"`
	Partitions []int32 `json:"partitions"`
	Group      string  `json:"group"`
	Size       int64   `json:"size"`
	Wait       int64   `json:"wait"`
	Multi      bool    `json:"multi"`
}

func (i *KafkaConf) ConfigName() string {
	return KafkaConfigName
}

var kafka_config = &KafkaConf{
	Connections: map[string]*KafkaConf_Connection{},
}
